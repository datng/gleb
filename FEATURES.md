# Features
This document lists the current and planned features of GLEB.
## Current features
- Reading embedded gltf files

## Planned high priority features
- [ ] Reading glb files

## Planned low priority features
- [ ] Saving gltf files
