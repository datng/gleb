# GLTF2.0
- [x] Basic Reader

## Usage of pointer to prevent duplication (top level array)
- [ ] scene
  - [ ] node
- [ ] node
  - [ ] node
  - [ ] skin
  - [ ] camera
  - [ ] mesh
- [ ] skin
  - [ ] accessor
- [ ] mesh
  - [ ] accessor
  - [ ] material
    - [ ] texture
      - [ ] image
      - [ ] sampler
- [ ] animation
  - [ ] accessor
- [ ] accessor
  - [ ] bufferView
    - [ ] buffer