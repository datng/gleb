function download_or_update {
    if [ -d $1 ]; then
        echo "Updating $1..."
        cd $1
        git pull
        if [ $? -ne 0 ]; then
            echo "Error updating $1! Downloading instead..."
            cd ..
            rm -rf $1
            download_or_update $1 $2
        else
            cd ..
        fi
    else
        git clone $2 || {
            echo "Error downloading $1!"
            exit 1
        }
        echo "Dependency \"$1\" from $2 downloaded."
    fi
}

download_or_update resources https://gitlab.com/KtnFramework/Libraries/gleb/resources.git
