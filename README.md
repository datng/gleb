# GLEB
A glTF loader/saver written in C++ 17.
- [Features](FEATURES.md)
- [Progress tracker](PROGRESS.md)

## Dependencies
- [rapidjson](https://github.com/Tencent/rapidjson)
