#ifndef GLEB_GLTF_PROPERTIES_TEXTUREINFO_HPP
#define GLEB_GLTF_PROPERTIES_TEXTUREINFO_HPP
#include "gltf_property.hpp"

namespace gleb::gltf_properties {
class textureInfo : gltf_property {
public:
    textureInfo(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    uint32_t index{}; // gltf_required

    uint32_t texCoord = 0;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_TEXTUREINFO_HPP
