#ifndef GLEB_GLTF_PROPERTIES_ASSET_HPP
#define GLEB_GLTF_PROPERTIES_ASSET_HPP
#include "gltf_property.hpp"

#include <optional>

namespace gleb {
namespace gltf_properties {
class asset_t : gltf_property {
public:
    asset_t(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<std::string> copyright;
    std::optional<std::string> generator;
    std::string                version; // gltf_required
    std::optional<std::string> minVersion;
};
} // namespace gltf_properties
} // namespace gleb
#endif // GLEB_GLTF_PROPERTIES_ASSET_HPP
