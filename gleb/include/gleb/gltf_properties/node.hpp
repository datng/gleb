#ifndef GLEB_GLTF_PROPERTIES_NODE_HPP
#define GLEB_GLTF_PROPERTIES_NODE_HPP
#include "gltf_property.hpp"

#include <array>
#include <optional>
#include <vector>

namespace gleb {
namespace gltf_properties {
class node : gltf_property {
public:
    node(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    static inline const std::array<float, 16> defaultMatrix      = {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
    static inline const std::array<float, 4>  defaultRotation    = {0, 0, 0, 1};
    static inline const std::array<float, 3>  defaultScale       = {1, 1, 1};
    static inline const std::array<float, 3>  defaultTranslation = {0, 0, 0};

    std::optional<uint32_t>    camera;
    std::vector<size_t>        children;
    std::optional<uint32_t>    skin;
    std::array<float, 16>      matrix = defaultMatrix;
    std::optional<uint32_t>    mesh;
    std::array<float, 4>       rotation    = defaultRotation;
    std::array<float, 3>       scale       = defaultScale;
    std::array<float, 3>       translation = defaultTranslation;
    std::vector<float>         weights;
    std::optional<std::string> name;
};
} // namespace gltf_properties
} // namespace gleb
#endif // GLEB_GLTF_PROPERTIES_NODE_HPP
