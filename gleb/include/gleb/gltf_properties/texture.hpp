#ifndef GLEB_GLTF_PROPERTIES_TEXTURE_HPP
#define GLEB_GLTF_PROPERTIES_TEXTURE_HPP
#include "gltf_property.hpp"

#include <optional>
#include <string>

namespace gleb::gltf_properties {
class texture : gltf_property {
public:
    texture(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<uint32_t>    sampler;
    std::optional<uint8_t>     source;
    std::optional<std::string> name;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_TEXTURE_HPP
