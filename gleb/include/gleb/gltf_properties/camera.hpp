#ifndef GLEB_GLTF_PROPERTIES_CAMERA_HPP
#define GLEB_GLTF_PROPERTIES_CAMERA_HPP
#include "gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb::gltf_properties {
class camera : gltf_property {
public:
    class camera_orthographic : gltf_property {
    public:
        camera_orthographic(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        float xmag{}; // gltf_required
        float ymag{}; // gltf_required
        float znear{}; // gltf_required
        float zfar{}; // gltf_required
    };

    class camera_perspective : gltf_property {
    public:
        camera_perspective(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        std::optional<float> aspectRatio;
        float                yfov{}; // gltf_required
        std::optional<float> zfar;
        float                znear{}; // gltf_required
    };

    camera(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    static inline const std::vector<std::string> validTypes{"perspective", "orthographic"};

    std::optional<camera_orthographic> orthographic;
    std::optional<camera_perspective>  perspective;
    std::string                        type; // gltf_required
    std::optional<std::string>         name;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_CAMERA_HPP
