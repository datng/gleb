#ifndef GLEB_GLTF_PROPERTIES_MESH_HPP
#define GLEB_GLTF_PROPERTIES_MESH_HPP
#include "gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb::gltf_properties {
class mesh : gltf_property {
public:
    class primitive : gltf_property {
    public:
        primitive(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
        primitive(const primitive &other) : gltf_property(other) {
            attributes.CopyFrom(other.attributes, attributes.GetAllocator());
            indices  = other.indices;
            material = other.material;
            mode     = other.mode;
            targets.CopyFrom(other.targets, targets.GetAllocator());
        }
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        // TODO: The standard is a bit unclear in here.
        // Is there a set of must-have attributes?
        // In the mean time, just copy the whole object and access like a map.
        rapidjson::Document     attributes; // gltf_required
        std::optional<uint32_t> indices;
        std::optional<uint32_t> material;

        static inline const std::vector<uint8_t> validModes = {
            0, // POINTS
            1, // LINES
            2, // LINE_LOOP
            3, // LINE_STRIP
            4, // TRIANGLES
            5, // TRIANGLE_STRIP
            6 // TRIANGLE_FAN
        };
        uint8_t mode = 4;

        // TODO: write a class for the morph target once it's clear what is exactly inside,
        // and if it's the same every time.
        rapidjson::Document targets;
    };

    mesh(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::vector<primitive>     primitives; // gltf_required
    std::vector<float>         weights;
    std::optional<std::string> name;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_MESH_HPP
