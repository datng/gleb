#ifndef GLEB_GLTF_PROPERTIES_IMAGE_HPP
#define GLEB_GLTF_PROPERTIES_IMAGE_HPP
#include "gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb::gltf_properties {
class image : gltf_property {
public:
    image(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    static inline const std::vector<std::string> validMimeTypes = {"image/jpeg", "image/png"};

    std::optional<std::string> uri;
    std::optional<std::string> mimeType;
    std::optional<uint32_t>    bufferView;
    std::optional<std::string> name;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_IMAGE_HPP
