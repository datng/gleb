#ifndef GLEB_GLTF_PROPERTIES_ACCESSOR_HPP
#define GLEB_GLTF_PROPERTIES_ACCESSOR_HPP
#include "gltf_property.hpp"

#include <optional>
#include <utility>
#include <vector>

namespace gleb {
namespace gltf_properties {
class accessor : gltf_property {
public:
    class sparse_t : gltf_property {
    public:
        class indices_t : gltf_property {
        public:
            indices_t(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
            void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

            static inline const std::vector<uint16_t> validComponentType = {
                5121, // UNSIGNED_BYTE
                5123, // UNSIGNED_SHORT
                5125 // UNSIGNED_INT
            };

            uint32_t bufferView{}; // gltf_required
            uint32_t byteOffset = 0;
            uint16_t componentType{}; // gltf_required
        };

        class values_t : gltf_property {
        public:
            values_t(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
            void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

            uint32_t bufferView{}; // gltf_required

            uint32_t byteOffset = 0;
        };

        sparse_t(std::string source, std::string id)
            : gltf_property(std::move(source), std::move(id)),
              indices(indices_t(Source, Identifier + ".indices")),
              values(values_t(Source, Identifier + ".values")) {}
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        uint32_t  count{}; // gltf_required
        indices_t indices; // gltf_required
        values_t  values; // gltf_required
    };

    accessor(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void                                      from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;
    static inline const std::vector<uint16_t> validComponentTypes = {
        5120, // BYTE
        5121, // UNSIGNED_BYTE
        5122, // SHORT
        5123, // UNSIGNED_SHORT
        5125, // UNSIGNED_INT
        5126 // FLOAT
    };
    static inline const std::vector<uint8_t>     validMaxMinLength = {1, 2, 3, 4, 9, 16};
    static inline const std::vector<std::string> validTypes        = {
        "SCALAR", "VEC2", "VEC3", "VEC4", "MAT2", "MAT3", "MAT4"};

    std::optional<uint32_t>    bufferView;
    uint32_t                   byteOffset = 0;
    uint16_t                   componentType{}; // gltf_required
    bool                       normalized = false;
    uint32_t                   count{}; // gltf_required
    std::string                type; // gltf_required
    std::vector<float>         max;
    std::vector<float>         min;
    std::optional<sparse_t>    sparse;
    std::optional<std::string> name;
};
} // namespace gltf_properties
} // namespace gleb
#endif // GLEB_GLTF_PROPERTIES_ACCESSOR_HPP
