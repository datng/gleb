#ifndef GLEB_GLTF_PROPERTIES_MATERIAL_HPP
#define GLEB_GLTF_PROPERTIES_MATERIAL_HPP
#include "gltf_property.hpp"
#include "textureInfo.hpp"

#include <optional>
#include <utility>
#include <vector>

namespace gleb {
namespace gltf_properties {
class material : gltf_property {
public:
    class normalTextureInfo : textureInfo {
    public:
        normalTextureInfo(const std::string &source, const std::string &id) : textureInfo(source, id) {}
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        float scale = 1.0f;
    };

    class occlusionTextureInfo : textureInfo {
    public:
        occlusionTextureInfo(const std::string &source, const std::string &id) : textureInfo(source, id) {}
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        float strength = 1.0f;
    };

    class pbrMetallicRoughness_t : gltf_property {
    public:
        pbrMetallicRoughness_t(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        std::array<float, 4>         baseColorFactor  = {1.0f, 1.0f, 1.0f, 1.0f};
        std::unique_ptr<textureInfo> baseColorTexture = nullptr;
        float                        metallicFactor   = 1;
        float                        roughnessFactor  = 1;
        rapidjson::Document          metallicRoughnessTexture;
    };

    material(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    static inline const std::vector<std::string> validAlphaModes = {"OPAQUE", "MASK", "BLEND"};

    std::optional<std::string>              name;
    std::shared_ptr<pbrMetallicRoughness_t> pbrMetallicRoughness = nullptr;
    std::shared_ptr<normalTextureInfo>      normalTexture        = nullptr;
    std::shared_ptr<occlusionTextureInfo>   occlusionTexture     = nullptr;
    std::optional<textureInfo>              emissiveTexture;
    std::array<float, 3>                    emissiveFactor = {0.0f, 0.0f, 0.0f};
    std::string                             alphaMode      = "OPAQUE";
    float                                   alphaCutoff    = 0.5;
    bool                                    doubleSided    = false;
};
} // namespace gltf_properties
} // namespace gleb
#endif // GLEB_GLTF_PROPERTIES_ACCESSOR_HPP
