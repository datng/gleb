#ifndef GLEB_GLTF_PROPERTIES_SCENE_HPP
#define GLEB_GLTF_PROPERTIES_SCENE_HPP
#include "gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
namespace gltf_properties {
class scene_t : gltf_property {
public:
    scene_t(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::vector<size_t>        nodes;
    std::optional<std::string> name;
};
} // namespace gltf_properties
} // namespace gleb

#endif // GLEB_GLTF_PROPERTIES_SCENE_HPP
