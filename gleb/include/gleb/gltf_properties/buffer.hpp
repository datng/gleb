#ifndef GLEB_GLTF_PROPERTIES_BUFFER_HPP
#define GLEB_GLTF_PROPERTIES_BUFFER_HPP
#include "gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb {
namespace gltf_properties {
class buffer : gltf_property {
public:
    buffer(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    [[nodiscard]] std::vector<char> data(const size_t &starting_index = 0, const size_t &length = SIZE_MAX) const;
    bool                            data_loaded();
    void                            load_data();
    void                            unload_data();

    std::optional<std::string> uri;
    size_t                     byteLength{}; // gltf_required
    std::optional<std::string> name;

private:
    std::vector<char> m_Data;
    bool              m_IsLoaded = false;
};
} // namespace gltf_properties
} // namespace gleb
#endif // GLEB_GLTF_PROPERTIES_BUFFER_HPP
