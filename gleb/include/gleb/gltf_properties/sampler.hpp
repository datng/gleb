#ifndef GLEB_GLTF_PROPERTIES_SAMPLER_HPP
#define GLEB_GLTF_PROPERTIES_SAMPLER_HPP
#include "gltf_property.hpp"

#include <optional>
#include <string>
#include <vector>

namespace gleb::gltf_properties {
class sampler : gltf_property {
public:
    sampler(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    static inline const std::vector<uint16_t> validMagFilters = {
        9728, // NEAREST
        9729 // LINEAR
    };

    static inline const std::vector<uint16_t> validMinFilters = {
        9728, // NEAREST
        9729, // LINEAR
        9984, // NEAREST_MIPMAP_NEAREST
        9985, // LINEAR_MIPMAP_NEAREST
        9986, // NEAREST_MIPMAP_LINEAR
        9987 // LINEAR_MIPMAP_LINEAR
    };

    static inline const std::vector<uint16_t> validWraps = {
        33071, // CLAMP_TO_EDGE
        33648, // MIRRORED_REPEAT
        10497 // REPEAT
    };

    std::optional<uint16_t>    magFilter;
    std::optional<uint16_t>    minFilter;
    uint16_t                   wrapS = 10497;
    uint16_t                   wrapT = 10497;
    std::optional<std::string> name;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_SAMPLER_HPP
