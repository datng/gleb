#ifndef GLEB_GLTF_PROPERTIES_SKIN_HPP
#define GLEB_GLTF_PROPERTIES_SKIN_HPP
#include "gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb::gltf_properties {
class skin : gltf_property {
public:
    skin(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::optional<uint32_t>    inverseBindMatrices;
    std::optional<uint32_t>    skeleton;
    std::vector<uint32_t>      joints; // gltf_required
    std::optional<std::string> name;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_SKIN_HPP
