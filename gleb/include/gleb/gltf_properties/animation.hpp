#ifndef GLEB_GLTF_PROPERTIES_ANIMATION_HPP
#define GLEB_GLTF_PROPERTIES_ANIMATION_HPP
#include "gltf_property.hpp"

#include <optional>
#include <vector>

namespace gleb::gltf_properties {
class animation : gltf_property {
public:
    class channel : gltf_property {
    public:
        class target_t : gltf_property {
        public:
            target_t(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
            void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

            static inline const std::vector<std::string> validPaths = {"translation", "rotation", "scale", "weights"};

            std::optional<uint32_t> node;
            std::string             path;
        };

        channel(std::string source, std::string id)
            : gltf_property(std::move(source), std::move(id)), //
              target(source, id + ".target") {}
        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        uint32_t sampler{}; // gltf_required
        target_t target; // gltf_required
    };

    class sampler : gltf_property {
    public:
        sampler(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}

        void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

        static inline const std::vector<std::string> validInterpolations = {"LINEAR", "STEP", "CUBICSPLINE"};

        uint32_t    input{}; // gltf_required
        std::string interpolation = "LINEAR";
        uint32_t    output{}; // gltf_required
    };
    animation(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    std::vector<channel>       channels; // gltf_required
    std::vector<sampler>       samplers; // gltf_required
    std::optional<std::string> name;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_ANIMATION_HPP
