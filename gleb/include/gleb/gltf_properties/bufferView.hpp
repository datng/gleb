#ifndef GLEB_GLTF_PROPERTIES_BUFFERVIEW_HPP
#define GLEB_GLTF_PROPERTIES_BUFFERVIEW_HPP
#include "gltf_property.hpp"

#include <optional>

namespace gleb {
namespace gltf_properties {
class bufferView : gltf_property {
public:
    bufferView(std::string source, std::string id) : gltf_property(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    uint32_t                   buffer{}; // gltf_required
    uint32_t                   byteOffset = 0;
    uint32_t                   byteLength{}; // gltf_required
    std::optional<uint8_t>     byteStride;
    std::optional<uint32_t>    target;
    std::optional<std::string> name;
};
} // namespace gltf_properties
} // namespace gleb
#endif // GLEB_GLTF_PROPERTIES_BUFFERVIEW_HPP
