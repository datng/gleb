#ifndef GLEB_GLTF_PROPERTIES_GLTF_PROPERTY_HPP
#define GLEB_GLTF_PROPERTIES_GLTF_PROPERTY_HPP
#include <rapidjson/document.h>

#include <memory>

namespace gleb::gltf_properties {
class property_base_t {
public:
    property_base_t(std::string source, std::string id) : Source(std::move(source)), Identifier(std::move(id)) {}
    virtual ~property_base_t()                                                    = default;
    virtual void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) = 0;
    // virtual void to_json() = 0;

protected:
    const std::string Source;
    const std::string Identifier;
};

class extensions_t : property_base_t {
public:
    extensions_t(std::string source, std::string id) : property_base_t(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override {
        Data.CopyFrom(doc, Data.GetAllocator());
    }
    rapidjson::Document Data;
};

class extras_t : property_base_t {
public:
    extras_t(std::string source, std::string id) : property_base_t(std::move(source), std::move(id)) {}
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override {
        Data.CopyFrom(doc, Data.GetAllocator());
    }

    rapidjson::Document Data;
};

class gltf_property : public property_base_t {
public:
    gltf_property(std::string source, std::string id) : property_base_t(std::move(source), std::move(id)) {}
    ~gltf_property() override = default;

    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override = 0;

    void check_and_read_extensions(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void check_and_read_extras(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);

    std::shared_ptr<extensions_t> extensions;
    std::shared_ptr<extras_t>     extras;
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_PROPERTIES_GLTF_PROPERTY_HPP
