#ifndef GLEB_GLTF_HPP
#define GLEB_GLTF_HPP
#include "gltf_properties/accessor.hpp"
#include "gltf_properties/animation.hpp"
#include "gltf_properties/asset.hpp"
#include "gltf_properties/buffer.hpp"
#include "gltf_properties/bufferView.hpp"
#include "gltf_properties/camera.hpp"
#include "gltf_properties/image.hpp"
#include "gltf_properties/material.hpp"
#include "gltf_properties/mesh.hpp"
#include "gltf_properties/node.hpp"
#include "gltf_properties/sampler.hpp"
#include "gltf_properties/scene.hpp"
#include "gltf_properties/skin.hpp"
#include "gltf_properties/texture.hpp"
#include "gltf_properties/utils.hpp"

#include <rapidjson/document.h>

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace gleb::gltf_properties {
class glTF : gltf_property {
public:
    explicit glTF(std::string source) : gltf_property(std::move(source), "") {}

    void set_path(const std::string &source, bool reload = true);

    void read();

    std::vector<gltf_properties::accessor>    accessors;
    std::vector<gltf_properties::animation>   animations;
    std::unique_ptr<gltf_properties::asset_t> asset = nullptr; // gltf_required
    std::vector<gltf_properties::buffer>      buffers;
    std::vector<gltf_properties::bufferView>  bufferViews;
    std::vector<gltf_properties::camera>      cameras;
    std::vector<gltf_properties::image>       images;
    std::vector<gltf_properties::material>    materials;
    std::vector<gltf_properties::mesh>        meshes;
    std::vector<gltf_properties::node>        nodes;
    std::vector<gltf_properties::sampler>     samplers;
    std::optional<size_t>                     scene;
    std::vector<gltf_properties::scene_t>     scenes;
    std::vector<gltf_properties::skin>        skins;
    std::vector<gltf_properties::texture>     textures;

private:
    void from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) override;

    void Clear() {
        // TODO: implement function to clear all members/set to default states
    }

    void ReadAccessors(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadAnimations(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadAsset(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadBuffers(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadBufferViews(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadCameras(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadImages(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadMaterials(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadMeshes(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadNodes(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadSamplers(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadSceneDefault(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadScenes(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadSkins(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
    void ReadTextures(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc);
};
} // namespace gleb::gltf_properties
#endif // GLEB_GLTF_HPP
