#define USE_GLEB_UTILS
#include <gleb/gltf_properties/skin.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kArrayType;
using rapidjson::Type::kNumberType;
using rapidjson::Type::kStringType;

namespace gleb::gltf_properties {
using namespace utils;

void skin::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "joints", kArrayType);

    joints.clear();
    for (size_t i = 0; i < doc["joints"].GetArray().Size(); ++i) { joints.emplace_back(doc["joints"][i].GetUint()); }

    if (check_existence(doc, "inverseBindMatrices", kNumberType)) {
        inverseBindMatrices = doc["inverseBindMatrices"].GetUint();
    }

    if (check_existence(doc, "skeleton", kNumberType)) { skeleton = doc["skeleton"].GetUint(); }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
