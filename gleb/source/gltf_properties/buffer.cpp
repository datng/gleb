#define USE_GLEB_UTILS
#include <gleb/gltf_properties/buffer.hpp>
#include <gleb/gltf_properties/utils.hpp>

#include <filesystem>
#include <fstream>

using rapidjson::Type::kNumberType;
using rapidjson::Type::kStringType;

namespace gleb {
namespace gltf_properties {
using namespace utils;

void buffer::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "byteLength", kNumberType);
    bool uriExists  = check_existence(doc, "uri", kStringType);
    bool nameExists = check_existence(doc, "name", kStringType);

    byteLength = doc["byteLength"].GetUint();
    if (byteLength < 1) { ReportAndCrash("Byte length must be >= 1."); }
    if (uriExists) { uri = doc["uri"].GetString(); }
    if (nameExists) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

std::vector<char> buffer::data(const size_t &starting_index, const size_t &length) const {
    if (starting_index + length >= m_Data.size() || length == SIZE_MAX) {
        return std::vector<char>(m_Data.begin() + starting_index, m_Data.end());
    }

    if (starting_index > m_Data.size()) { return std::vector<char>{}; }

    return std::vector<char>(m_Data.begin() + starting_index, m_Data.begin() + starting_index + length);
}

bool buffer::data_loaded() {
    return m_IsLoaded;
}

void buffer::load_data() {
    // load data from .bin file
    if (uri.has_value()) {
        if (uri.value().substr(uri.value().size() - 4, 4) != ".bin") {
            // TODO: add support for embedded buffers
            throw std::invalid_argument("The uri does not point to a .bin file.");
        }

        std::filesystem::path parent_path = std::filesystem::path(Source).parent_path();

        std::ifstream           stream(parent_path.string() + "/" + uri.value(), std::ios::binary | std::ios::ate);
        std::ifstream::pos_type pos = stream.tellg();
        m_Data.resize(pos);

        stream.seekg(0, std::ios::beg);
        stream.read(&m_Data[0], pos);
    }
    m_IsLoaded = true;
}

void buffer::unload_data() {
    m_IsLoaded = false;
    // TODO: write the unload function to erase data
}
} // namespace gltf_properties
} // namespace gleb
#undef USE_GLEB_UTILS
