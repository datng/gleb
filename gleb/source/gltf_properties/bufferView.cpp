#define USE_GLEB_UTILS
#include <gleb/gltf_properties/bufferView.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kNumberType;
using rapidjson::Type::kStringType;

using std::to_string;

namespace gleb {
namespace gltf_properties {
using namespace utils;
void bufferView::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "buffer", kNumberType);
    AssertExistence(doc, "byteLength", kNumberType);
    bool byteOffsetExists = check_existence(doc, "byteOffset", kNumberType);
    bool byteStrideExists = check_existence(doc, "byteStride", kNumberType);
    bool targetExists     = check_existence(doc, "target", kNumberType);
    bool nameExists       = check_existence(doc, "name", kStringType);

    buffer     = doc["buffer"].GetUint();
    byteLength = doc["byteLength"].GetUint();
    if (byteLength == 0) { ReportAndCrash("Byte length must be positive."); }
    if (byteOffsetExists) { byteOffset = doc["byteOffset"].GetUint(); }
    if (byteStrideExists) {
        byteStride = doc["byteStride"].GetUint();
        if (byteStride < 4 || byteStride > 252) { ReportAndCrash("Byte stride has to be in the range of [4, 252]."); }
    }
    if (targetExists) {
        target                              = doc["target"].GetUint();
        constexpr uint ARRAY_BUFFER         = 34962;
        constexpr uint ELEMENT_ARRAY_BUFFER = 34963;

        if (target != ARRAY_BUFFER && target != ELEMENT_ARRAY_BUFFER) {
            std::string msg = "bufferView target has to be either " + to_string(ARRAY_BUFFER) + "(ARRAY_BUFFER)" //
                              + " or " + to_string(ELEMENT_ARRAY_BUFFER) + "(ELEMENT_ARRAY_BUFFER).";
            FileReadError(Source, Identifier, msg);
        }
    }
    if (nameExists) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gltf_properties
} // namespace gleb
#undef USE_GLEB_UTILS
