#define USE_GLEB_UTILS
#include <gleb/gltf_properties/gltf_property.hpp>
#include <gleb/gltf_properties/utils.hpp>

namespace gleb::gltf_properties {
using namespace utils;
void gltf_property::check_and_read_extensions(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    if (check_existence(doc, "extensions", rapidjson::Type::kObjectType)) {
        extensions = std::make_shared<extensions_t>(Source, Identifier + ".extensions");
        extensions->from_json(doc["extensions"]);
    }
}

void gltf_property::check_and_read_extras(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    if (check_existence(doc, "extras")) {
        extras = std::make_shared<extras_t>(Source, Identifier + ".extras");
        extras->from_json(doc["extras"]);
    }
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
