#define USE_GLEB_UTILS
#include <gleb/gltf_properties/accessor.hpp>
#include <gleb/gltf_properties/utils.hpp>

#include <algorithm>
using rapidjson::Type::kNumberType;
using rapidjson::Type::kObjectType;
using rapidjson::Type::kStringType;

using std::find;

namespace gleb::gltf_properties {
using namespace utils;
using sparse_t  = accessor::sparse_t;
using indices_t = sparse_t::indices_t;
using values_t  = sparse_t::values_t;

void indices_t::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "bufferView", kNumberType);
    AssertExistence(doc, "componentType", kObjectType);

    bool byteOffsetExists = check_existence(doc, "byteOffset", kNumberType);

    bufferView    = doc["bufferView"].GetUint();
    componentType = doc["componentType"].GetUint();
    if (std::find(validComponentType.begin(), validComponentType.end(), componentType) == validComponentType.end()) {
        FileReadError(Source, Identifier, "Invalid component type.");
    }
    if (byteOffsetExists) { byteOffset = doc["byteOffset"].GetUint(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void values_t::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "bufferView", kNumberType);
    bufferView = doc["bufferView"].GetUint();

    if (check_existence(doc, "byteOffset", kNumberType)) { byteOffset = doc["byteOffset"].GetUint(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void sparse_t::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "count", kNumberType);
    AssertExistence(doc, "indices", kObjectType);
    AssertExistence(doc, "values", kObjectType);

    count = doc["count"].GetUint();
    indices.from_json(doc["indices"]);
    values.from_json(doc["values"]);
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void accessor::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "componentType", kNumberType);
    AssertExistence(doc, "count", kNumberType);
    AssertExistence(doc, "type", kStringType);
    bool bufferViewExists = check_existence(doc, "bufferView", kNumberType);
    bool byteOffsetExists = check_existence(doc, "byteOffset", kNumberType);
    bool normalizedExists = check_existence_bool(doc, "normalized");
    bool maxExists        = check_existence_array(doc, "max");
    bool minExists        = check_existence_array(doc, "min");
    bool sparseExists     = check_existence(doc, "sparse", kObjectType);
    bool nameExists       = check_existence(doc, "name", kStringType);

    componentType = doc["componentType"].GetUint();
    if (find(validComponentTypes.begin(), validComponentTypes.end(), componentType) == validComponentTypes.end()) {
        ReportAndCrash("Component type is not valid.");
    }

    count = doc["count"].GetUint();
    if (count == 0) { ReportAndCrash("Count must be >= 1."); }

    type = doc["type"].GetString();
    if (find(validTypes.begin(), validTypes.end(), type) == validTypes.end()) {
        ReportAndCrash("Accessor type is not valid.");
    }

    if (bufferViewExists) { bufferView = doc["bufferView"].GetUint(); }

    if (byteOffsetExists) { byteOffset = doc["byteOffset"].GetUint(); }

    if (normalizedExists) { normalized = doc["normalized"].GetBool(); }

    if (maxExists) {
        AssertArrayOfNumbers(doc, "max");
        max = std::vector<float>();
        for (auto &temp : doc["max"].GetArray()) { max.push_back(temp.GetFloat()); }
        if (find(validMaxMinLength.begin(), validMaxMinLength.end(), max.size()) == validMaxMinLength.end()) {
            ReportAndCrash("Assessor max array has an invalid number of elements.");
        }
    }

    if (minExists) {
        AssertArrayOfNumbers(doc, "min");
        min = std::vector<float>();
        for (auto &temp : doc["min"].GetArray()) { min.push_back(temp.GetFloat()); }
        if (find(validMaxMinLength.begin(), validMaxMinLength.end(), min.size()) == validMaxMinLength.end()) {
            ReportAndCrash("Assessor min array has an invalid number of elements.");
        }
    }

    if (sparseExists) { sparse->from_json(doc["sparse"]); }

    if (nameExists) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
