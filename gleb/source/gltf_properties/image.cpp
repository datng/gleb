#define USE_GLEB_UTILS
#include <gleb/gltf_properties/image.hpp>
#include <gleb/gltf_properties/utils.hpp>

#include <algorithm>

using rapidjson::Type::kNumberType;
using rapidjson::Type::kStringType;

namespace gleb::gltf_properties {
using namespace utils;
void image::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    if (check_existence(doc, "uri", kNumberType)) { uri = doc["uri"].GetString(); }
    if (check_existence(doc, "mimeType", kNumberType)) {
        mimeType = doc["mimeType"].GetString();
        if (std::find(validMimeTypes.begin(), validMimeTypes.end(), mimeType) == validMimeTypes.end()) {
            FileReadError(Source, Identifier, "Invalid mime type.");
        }
    }
    if (check_existence(doc, "bufferView", kNumberType)) { bufferView = doc["bufferView"].GetUint(); }
    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
