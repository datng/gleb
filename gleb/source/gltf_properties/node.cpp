#define USE_GLEB_UTILS
#include <gleb/gltf_properties/node.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kArrayType;
using rapidjson::Type::kNumberType;
using rapidjson::Type::kStringType;

namespace gleb::gltf_properties {
using namespace utils;
void node::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    children.clear();
    matrix      = defaultMatrix;
    rotation    = defaultRotation;
    scale       = defaultScale;
    translation = defaultTranslation;
    weights.clear();

    bool cameraExists      = check_existence(doc, "camera", kNumberType);
    bool childrenExists    = check_existence(doc, "children", kArrayType);
    bool skinExists        = check_existence(doc, "skin", kNumberType);
    bool matrixExists      = check_existence(doc, "matrix", kArrayType);
    bool meshExists        = check_existence(doc, "mesh", kNumberType);
    bool rotationExists    = check_existence(doc, "rotation", kArrayType);
    bool scaleExists       = check_existence(doc, "scale", kArrayType);
    bool translationExists = check_existence(doc, "translation", kArrayType);
    bool weightsExists     = check_existence(doc, "weights", kArrayType);
    bool nameExists        = check_existence(doc, "camera", kStringType);

    if (cameraExists) { camera = doc["camera"].GetUint(); }

    if (childrenExists) {
        for (auto &child : doc["children"].GetArray()) { children.emplace_back(child.GetUint()); }
    }

    if (skinExists) { skin = doc["skin"].GetUint(); }

    if (matrixExists) {
        if (doc["matrix"].GetArray().Size() != 16) {
            FileReadError(Source, Identifier, "Invalid transformation matrix size.");
        }
        for (unsigned short i = 0; i < 16; ++i) { matrix.at(i) = doc["matrix"][i].GetFloat(); }
    }

    if (meshExists) { mesh = doc["mesh"].GetUint(); }

    if (rotationExists) {
        if (doc["rotation"].GetArray().Size() != 4) {
            FileReadError(Source, Identifier, "Invalid rotation array size.");
        }
        for (unsigned short i = 0; i < 4; ++i) { rotation.at(i) = doc["rotation"][i].GetFloat(); }
    }

    if (scaleExists) {
        if (doc["scale"].GetArray().Size() != 3) { FileReadError(Source, Identifier, "Invalid scale array size."); }
        for (unsigned short i = 0; i < 3; ++i) { scale.at(i) = doc["scale"][i].GetFloat(); }
    }

    if (translationExists) {
        if (doc["translation"].GetArray().Size() != 3) {
            FileReadError(Source, Identifier, "Invalid translation array size.");
        }
        for (unsigned short i = 0; i < 3; ++i) { translation.at(i) = doc["translation"][i].GetFloat(); }
    }

    if (weightsExists) {
        for (auto &weight : doc["weights"].GetArray()) { weights.emplace_back(weight.GetUint()); }
    }

    if (nameExists) { name = doc["name"].GetString(); }
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
