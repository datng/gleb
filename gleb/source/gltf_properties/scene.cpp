#define USE_GLEB_UTILS
#include <gleb/gltf_properties/scene.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kArrayType;
using rapidjson::Type::kStringType;

namespace gleb {
namespace gltf_properties {
using namespace utils;

void scene_t::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    if (check_existence(doc, "nodes", kArrayType)) {
        nodes.clear();
        for (auto &node : doc["nodes"].GetArray()) { nodes.emplace_back(node.GetUint()); }
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gltf_properties
} // namespace gleb
#undef USE_GLEB_UTILS
