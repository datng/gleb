#define USE_GLEB_UTILS
#include <gleb/gltf_properties/asset.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kStringType;

namespace gleb {
namespace gltf_properties {
using namespace utils;
void asset_t::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "version", kStringType);
    bool copyrightExists  = check_existence(doc, "copyright", kStringType);
    bool generatorExists  = check_existence(doc, "generator", kStringType);
    bool minVersionExists = check_existence(doc, "minVersion", kStringType);

    version = doc["version"].GetString();
    if (copyrightExists) { copyright = doc["copyright"].GetString(); }
    if (generatorExists) { generator = doc["generator"].GetString(); }
    if (minVersionExists) { minVersion = doc["minVersion"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gltf_properties
} // namespace gleb
#undef USE_GLEB_UTILS
