#define USE_GLEB_UTILS
#include <gleb/gltf_properties/mesh.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kArrayType;
using rapidjson::Type::kNumberType;
using rapidjson::Type::kObjectType;
using rapidjson::Type::kStringType;

using std::to_string;

namespace gleb::gltf_properties {
using namespace utils;

void mesh::primitive::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "attributes", kObjectType);
    attributes.CopyFrom(doc["attributes"], attributes.GetAllocator());

    if (check_existence(doc, "indices", kNumberType)) { indices = doc["indices"].GetUint(); }

    if (check_existence(doc, "material", kNumberType)) { material = doc["material"].GetUint(); }

    if (check_existence(doc, "mode", kNumberType)) {
        mode = doc["mode"].GetUint();
        if (!check_validity(mode, validModes)) { FileReadError(Source, Identifier, "Invalid mesh primitive mode."); }
    }

    if (check_existence(doc, "targets", kArrayType)) { targets.CopyFrom(doc["targets"], targets.GetAllocator()); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void mesh::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "primitives", kArrayType);
    primitives.clear();
    for (size_t i = 0; i < doc["primitives"].GetArray().Size(); ++i) {
        primitive temp(Source, Identifier + ".primitives[" + to_string(i) + "]");
        temp.from_json(doc["primitives"][i]);
        primitives.emplace_back(temp);
    }

    weights.clear();
    if (check_existence(doc, "weights", kArrayType)) {
        for (size_t i = 0; i < doc["weights"].GetArray().Size(); ++i) {
            if (!doc["weights"][i].IsNumber()) {
                FileReadError(Source, Identifier, "The value in weights[" + to_string(i) + "] is not a number.");
            }
            weights.emplace_back(doc["weights"][i].GetFloat());
        }
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
