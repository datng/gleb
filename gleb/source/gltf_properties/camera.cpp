#define USE_GLEB_UTILS
#include <gleb/gltf_properties/camera.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kNumberType;
using rapidjson::Type::kObjectType;
using rapidjson::Type::kStringType;

namespace gleb::gltf_properties {
using namespace utils;
void camera::camera_orthographic::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "xmag", kNumberType);
    AssertExistence(doc, "ymag", kNumberType);
    AssertExistence(doc, "zfar", kNumberType);
    AssertExistence(doc, "znear", kNumberType);

    xmag  = doc["xmag"].GetFloat();
    ymag  = doc["ymag"].GetFloat();
    zfar  = doc["zfar"].GetFloat();
    znear = doc["znear"].GetFloat();
    if (zfar <= 0) { FileReadError(Source, Identifier, "Invalid zfar value."); }
    if (znear < 0) { FileReadError(Source, Identifier, "Invalid znear value."); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void camera::camera_perspective::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "yfov", kNumberType);
    AssertExistence(doc, "znear", kNumberType);

    yfov = doc["yfov"].GetFloat();
    if (yfov <= 0) { FileReadError(Source, Identifier, "Invalid yfov value."); }

    znear = doc["znear"].GetFloat();
    if (znear <= 0) { FileReadError(Source, Identifier, "Invalid znear value."); }

    if (check_existence(doc, "aspectRatio", kNumberType)) {
        aspectRatio = doc["aspectRatio"].GetFloat();
        if (aspectRatio.value() <= 0) { FileReadError(Source, Identifier, "Invalid aspect ratio."); }
    }

    if (check_existence(doc, "zfar", kNumberType)) {
        zfar = doc["zfar"].GetFloat();
        if (zfar.value() <= 0) { FileReadError(Source, Identifier, "Invalid zfar value."); }
    }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void camera::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "type", kStringType);

    type = doc["type"].GetString();
    if (!check_validity(type, validTypes)) { FileReadError(Source, Identifier, "Invalid camera type"); }

    if (check_existence(doc, "orthographic", kObjectType)) {
        orthographic.emplace(camera_orthographic(Source, Identifier + ".orthographic"));
        orthographic->from_json(doc["orthographic"]);
    }

    if (check_existence(doc, "perspective", kObjectType)) {
        perspective.emplace(camera_perspective(Source, Identifier + ".perspective"));
        perspective->from_json(doc["perspective"]);
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
