#define USE_GLEB_UTILS
#include <gleb/gltf_properties/sampler.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kNumberType;
using rapidjson::Type::kStringType;

namespace gleb::gltf_properties {
using namespace utils;

void sampler::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    if (check_existence(doc, "magFilter", kNumberType)) {
        magFilter = doc["magFilter"].GetUint();
        if (!check_validity(magFilter.value(), validMagFilters)) {
            FileReadError(Source, Identifier, "Invalid magnification filter.");
        }
    }
    if (check_existence(doc, "minFilter", kNumberType)) {
        minFilter = doc["minFilter"].GetUint();
        if (!check_validity(minFilter.value(), validMinFilters)) {
            FileReadError(Source, Identifier, "Invalid minification filter.");
        }
    }

    if (check_existence(doc, "wrapS", kNumberType)) {
        wrapS = doc["wrapS"].GetUint();
        if (!check_validity(wrapS, validWraps)) { FileReadError(Source, Identifier, "Invalid wrapS value."); }
    }

    if (check_existence(doc, "wrapT", kNumberType)) {
        wrapT = doc["wrapT"].GetUint();
        if (!check_validity(wrapT, validWraps)) { FileReadError(Source, Identifier, "Invalid wrapT value."); }
    }

    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
