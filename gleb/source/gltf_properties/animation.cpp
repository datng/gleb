#define USE_GLEB_UTILS
#include <gleb/gltf_properties/animation.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kNumberType;
using rapidjson::Type::kObjectType;
using rapidjson::Type::kStringType;

using std::to_string;

namespace gleb::gltf_properties {
using namespace utils;
using target_t = animation::channel::target_t;
using channel  = animation::channel;
using sampler  = animation::sampler;

void target_t::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "path", kStringType);
    path = doc["path"].GetString();
    if (!check_validity(path, validPaths)) {
        FileReadError(Source, Identifier, "Invalid animation channel target path.");
    }
    if (check_existence(doc, "node", kNumberType)) { node = doc["node"].GetUint(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void channel::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "sampler", kNumberType);
    AssertExistence(doc, "target", kObjectType);

    sampler = doc["sampler"].GetUint();
    target.from_json(doc["target"]);

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void sampler::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "input", kNumberType);
    AssertExistence(doc, "output", kNumberType);

    input  = doc["input"].GetUint();
    output = doc["output"].GetUint();

    if (check_existence(doc, "interpolation", kStringType)) {
        interpolation = doc["interpolation"].GetString();
        if (!check_validity(interpolation, validInterpolations)) {
            FileReadError(Source, Identifier, "Invalid interpolation.");
        }
    }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}

void animation::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "channels", kObjectType);
    AssertExistence(doc, "samplers", kObjectType);

    channels.clear();
    for (size_t i = 0; i < doc["channels"].GetArray().Size(); ++i) {
        channel temp(Source, Identifier + ".channels[" + to_string(i) + "]");
        temp.from_json(doc["channels"][i]);
        channels.emplace_back(temp);
    }

    samplers.clear();
    for (size_t i = 0; i < doc["samplers"].GetArray().Size(); ++i) {
        sampler temp(Source, Identifier + ".samplers[" + to_string(i) + "]");
        temp.from_json(doc["samplers"][i]);
        samplers.emplace_back(temp);
    }

    if (utils::check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }

    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
