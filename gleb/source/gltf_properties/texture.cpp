#define USE_GLEB_UTILS
#include <gleb/gltf_properties/texture.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kNumberType;
using rapidjson::Type::kStringType;

namespace gleb::gltf_properties {
using utils::check_existence;

void texture::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    if (check_existence(doc, "sampler", kNumberType)) { sampler = doc["sampler"].GetUint(); }
    if (check_existence(doc, "source", kNumberType)) { source = doc["source"].GetUint(); }
    if (check_existence(doc, "name", kStringType)) { name = doc["name"].GetString(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
