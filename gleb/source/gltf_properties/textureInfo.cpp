#define USE_GLEB_UTILS
#include <gleb/gltf_properties/textureInfo.hpp>
#include <gleb/gltf_properties/utils.hpp>

using rapidjson::Type::kNumberType;

namespace gleb::gltf_properties {
using utils::check_existence;

void textureInfo::from_json(const rapidjson::GenericValue<rapidjson::UTF8<>> &doc) {
    AssertExistence(doc, "index", kNumberType);
    index = doc["index"].GetUint();

    if (check_existence(doc, "texCoord", kNumberType)) { texCoord = doc["texCoord"].GetUint(); }
    check_and_read_extensions(doc);
    check_and_read_extras(doc);
}
} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
