#define USE_GLEB_UTILS
#include <gleb/gltf.hpp>

using gleb::gltf_properties::accessor;
using gleb::gltf_properties::animation;
using gleb::gltf_properties::asset_t;
using gleb::gltf_properties::buffer;
using gleb::gltf_properties::bufferView;
using gleb::gltf_properties::camera;
using gleb::gltf_properties::image;
using gleb::gltf_properties::mesh;
using gleb::gltf_properties::node;
using gleb::gltf_properties::sampler;
using gleb::gltf_properties::scene_t;
using gleb::gltf_properties::skin;
using gleb::gltf_properties::texture;
using gleb::gltf_properties::utils::check_existence;
using gleb::gltf_properties::utils::LocationReport;

using rapidjson::Document;
using rapidjson::GenericValue;
using rapidjson::UTF8;
using rapidjson::Type::kArrayType;
using rapidjson::Type::kNumberType;
using rapidjson::Type::kObjectType;

using std::cout;
using std::endl;
using std::runtime_error;
using std::string;
using std::to_string;

namespace gleb::gltf_properties {
void glTF::set_path(const string &path, bool reload) {
    Clear();
    if (reload) { read(); }
}

void glTF::from_json(const GenericValue<UTF8<>> &doc) {
    ReadAccessors(doc);
    ReadAnimations(doc);
    ReadAsset(doc);
    ReadBuffers(doc);
    ReadBufferViews(doc);
    ReadCameras(doc);
    ReadImages(doc);
    ReadMaterials(doc);
    ReadMeshes(doc);
    ReadNodes(doc);
    ReadSamplers(doc);
    ReadSceneDefault(doc);
    ReadScenes(doc);
    ReadSkins(doc);
    ReadTextures(doc);
}

void glTF::read() {
    if (Source.empty()) { throw runtime_error(LocationReport(__FILE__, __LINE__, "m_Path is empty.")); }

    string        content;
    string        line;
    std::ifstream file;
    file.open(Source);
    if (!file.is_open()) { throw runtime_error(LocationReport(__FILE__, __LINE__, "Could not open " + Source + ".")); }
    while (getline(file, line)) { content += line; }
    Document document;
    document.Parse(content.c_str());
    from_json(document);
}

void glTF::ReadAccessors(const GenericValue<UTF8<>> &doc) {
    accessors.clear();
    const bool accessorsExists = check_existence(doc, "accessors", kArrayType);
    if (!accessorsExists) { return; }
    for (size_t i = 0; i < doc["accessors"].GetArray().Size(); ++i) {
        accessor temp(Source, "accessors[" + to_string(i) + "]");
        temp.from_json(doc["accessors"][i]);
        accessors.emplace_back(std::move(temp));
    }
}

void glTF::ReadAnimations(const GenericValue<UTF8<>> &doc) {
    animations.clear();
    const bool accessorsExists = check_existence(doc, "animations", kArrayType);
    if (!accessorsExists) { return; }
    for (size_t i = 0; i < doc["animations"].GetArray().Size(); ++i) {
        animation temp(Source, "animations[" + to_string(i) + "]");
        temp.from_json(doc["animations"][i]);
        animations.emplace_back(std::move(temp));
    }
}

void glTF::ReadAsset(const GenericValue<UTF8<>> &doc) {
    asset = std::make_unique<asset_t>(Source, "asset");
    if (!check_existence(doc, "asset", kObjectType)) { FileReadError(Source, "glTF", "Asset not found"); }
    asset->from_json(doc["asset"]);
}

void glTF::ReadBuffers(const GenericValue<UTF8<>> &doc) {
    buffers.clear();
    if (!check_existence(doc, "buffers", kArrayType)) { return; }
    for (size_t i = 0; i < doc["buffers"].GetArray().Size(); ++i) {
        buffer temp(Source, "buffers[" + to_string(i) + "]");
        temp.from_json(doc["buffers"][i]);
        buffers.emplace_back(std::move(temp));
    }

    for (auto &buffer : buffers) { buffer.load_data(); }
}

void glTF::ReadBufferViews(const GenericValue<UTF8<>> &eDocument) {
    bufferViews.clear();
    if (!check_existence(eDocument, "bufferViews", kArrayType)) { return; }
    for (size_t i = 0; i < eDocument["bufferViews"].GetArray().Size(); ++i) {
        bufferView temp(Source, "bufferViews[" + to_string(i) + "]");
        temp.from_json(eDocument["bufferViews"][i]);
        bufferViews.emplace_back(std::move(temp));
    }
}

void glTF::ReadCameras(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "cameras", kArrayType)) { return; }
    cameras.clear();
    for (size_t i = 0; i < doc["cameras"].GetArray().Size(); ++i) {
        camera temp(Source, "cameras[" + to_string(i) + "]");
        temp.from_json(doc["cameras"][i]);
        cameras.emplace_back(temp);
    }
}

void glTF::ReadImages(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "images", kArrayType)) { return; }
    images.clear();
    for (size_t i = 0; i < doc["images"].GetArray().Size(); ++i) {
        image temp(Source, "images[" + to_string(i) + "]");
        temp.from_json(doc["images"][i]);
        images.emplace_back(temp);
    }
}

void glTF::ReadMaterials(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "materials", kArrayType)) { return; }
    materials.clear();
    for (size_t i = 0; i < doc["materials"].GetArray().Size(); ++i) {
        material temp(Source, "materials[" + to_string(i) + "]");
        temp.from_json(doc["materials"][i]);
        materials.emplace_back(temp);
    }
}

void glTF::ReadMeshes(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "meshes", kArrayType)) { return; }
    meshes.clear();
    for (size_t i = 0; i < doc["meshes"].GetArray().Size(); ++i) {
        mesh temp(Source, "meshes[" + to_string(i) + "]");
        temp.from_json(doc["meshes"][i]);
        meshes.emplace_back(temp);
    }
}

void glTF::ReadNodes(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "nodes", kArrayType)) { return; }
    nodes.clear();
    for (size_t i = 0; i < doc["nodes"].GetArray().Size(); ++i) {
        node temp(Source, "nodes[" + to_string(i) + "]");
        temp.from_json(doc["nodes"][i]);
        nodes.emplace_back(temp);
    }
}

void glTF::ReadSceneDefault(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "scene", kNumberType)) { return; }
    scene = doc["scene"].GetUint();
}

void glTF::ReadScenes(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "scenes", kArrayType)) { return; }
    scenes.clear();
    for (size_t i = 0; i < doc["scenes"].GetArray().Size(); ++i) {
        scene_t temp(Source, "scenes[" + to_string(i) + "]");
        temp.from_json(doc["scenes"][i]);
        scenes.emplace_back(temp);
    }
}

void glTF::ReadSkins(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "skins", kArrayType)) { return; }
    skins.clear();
    for (size_t i = 0; i < doc["skins"].GetArray().Size(); ++i) {
        skin temp(Source, "skins[" + to_string(i) + "]");
        temp.from_json(doc["skins"][i]);
        skins.emplace_back(temp);
    }
}

void glTF::ReadSamplers(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "samplers", kArrayType)) { return; }
    samplers.clear();
    for (size_t i = 0; i < doc["samplers"].GetArray().Size(); ++i) {
        sampler temp(Source, "samplers[" + to_string(i) + "]");
        temp.from_json(doc["samplers"][i]);
        samplers.emplace_back(temp);
    }
}

void glTF::ReadTextures(const GenericValue<UTF8<>> &doc) {
    if (!check_existence(doc, "textures", kArrayType)) { return; }
    textures.clear();
    for (size_t i = 0; i < doc["textures"].GetArray().Size(); ++i) {
        texture temp(Source, "textures[" + to_string(i) + "]");
        temp.from_json(doc["textures"][i]);
        textures.emplace_back(temp);
    }
}

} // namespace gleb::gltf_properties
#undef USE_GLEB_UTILS
